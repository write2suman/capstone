FROM tomcat:8

LABEL maintainer="suman.sinha@wipro.com"

ADD /artifact/bookstore.war /usr/local/tomcat/webapps/

EXPOSE 8080

CMD ["catalina.sh","run"]

