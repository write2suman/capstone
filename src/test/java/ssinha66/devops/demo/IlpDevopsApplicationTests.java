package ssinha66.devops.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ssinha66.devops.demo.model.UserAccount;
import ssinha66.devops.demo.service.LoginService;

@SpringBootTest
class IlpDevopsApplicationTests extends AbstractTest{

	@Test
	void contextLoads() {
	}
	
	public void setUp() {
		super.setUp();
	}
	
	@Autowired
	private LoginService service;
	
	@Test
	public void testRegistration() throws Exception {
		UserAccount user = new UserAccount();
		user.setUserName("suman");
		user.setPassword("Suman@123");
		user.setEmail("suman.sinha@wipro.com");
		user.setMobile("9830404512");
		UserAccount createdUser = service.registerUser(user);
		assertEquals("suman", createdUser.getUserName());
		assertEquals("suman.sinha@wipro.com", createdUser.getEmail());
	}
	@Test
	public void testLogin() throws Exception {
		UserAccount user = new UserAccount();
		user.setUserName("suman");
		user.setPassword("Suman@123");
		user.setEmail("ssinha@qatarposts.com");
		user.setMobile("9830404512");
		UserAccount createdUser = service.registerUser(user);
		boolean validateUser = service.validateUser("suman", "Suman@123");
		assertEquals("true",String.valueOf(validateUser));
	}
	
	@Test
	public void testForgetPassword() throws Exception {
		UserAccount userFound = service.findPasswordByEmail("ssinha@qatarposts.com");
		userFound.setPassword("Newpasss@123");
		UserAccount userUpdated =service.updatePassword(userFound);
		assertEquals("Newpasss@123",userUpdated.getPassword());
	}

}
