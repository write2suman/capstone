<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<style>
body {
	font-family: Calibri, Helvetica, sans-serif;
}

.container {
	padding: 25px;
	background-color: lightblue;
	font-size: 50px;
}
</style>
<head>
<meta charset="UTF-8">
<title>Welcome Page</title>
</head>
<body>
  <div class="container">
	Welcome to online bookstore ${name}!! 
</div>
</body>
</html>