<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<style>
body {
	font-family: Calibri, Helvetica, sans-serif;
	background-color: pink;
}

button {
	background-color: #356adc;
	width: 100%;
	color: orange;
	padding: 15px;
	margin: 10px 0px;
	border: none;
	cursor: pointer;
	font-size: 40px;
}

form {
	border: 3px solid #f1f1f1;
}

input[type=text], input[type=password] {
	width: 100%;
	margin: 8px 0;
	padding: 12px 20px;
	display: inline-block;
	border: 2px solid green;
	box-sizing: border-box;
}

button:hover {
	opacity: 0.7;
}

.container {
	padding: 25px;
	background-color: lightblue;
}
</style>
<head>
<meta charset="UTF-8">
<title>Registration</title>
</head>
<body>
	<div>
		<center>
		<h2>This is registration form of Bookstore</h2>
		</center>
	</div>
	<form method="post">
		<div class="container">
			<label>Username : </label> <input type="text"
				placeholder="Enter Username" name="name" required> 
				<label>Password: </label> 
				<input type="password" placeholder="Enter Password"
				name="password" required> <br> 
				<label>Email: </label> 
				 <input type="text" placeholder="Enter Email"
				name="email" required> <br>  <label>Mobile: </label> <input type="text" placeholder="Enter Mobile Number"
				name="mobile" required> <br> 
				
				<button type="submit">Register</button>
				<br> <font color="red">${validationError}</font>
		</div>
	</form>
</body>
</html>