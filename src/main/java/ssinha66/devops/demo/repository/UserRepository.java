package ssinha66.devops.demo.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ssinha66.devops.demo.model.UserAccount;

@Repository
public interface UserRepository extends CrudRepository<UserAccount, Long> {
	
	@Query("SELECT user FROM USERACCOUNT user where user.userName=:userName and user.password=:password")
	UserAccount findByCredentials(String userName,String password);
	
	@Query("SELECT p FROM USERACCOUNT p where p.email=:email")
	UserAccount findByEmail(String email);

}
