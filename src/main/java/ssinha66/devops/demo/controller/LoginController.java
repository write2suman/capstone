package ssinha66.devops.demo.controller;

import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import ssinha66.devops.demo.model.UserAccount;
import ssinha66.devops.demo.service.LoginService;

@Controller
@SessionAttributes("name")
public class LoginController {

	@Autowired
	LoginService service;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String showLoginPage(ModelMap model) {
		return "login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String showWelcomePage(ModelMap model, @RequestParam String name, @RequestParam String password) {

		boolean isValidUser = service.validateUser(name, password);

		if (!isValidUser) {
			model.put("errorMessage", "Invalid Credentials, please enter correct one..");
			return "login";
		}

		model.put("name", name);
		model.put("password", password);

		return "welcome";
	}

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String showRegsiterPage(ModelMap model) {
		return "registration";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ModelAndView registerUser(ModelMap model, @RequestParam String name, @RequestParam String password,
			@RequestParam String email, @RequestParam String mobile) {

		if(!emailValidate(email)) {
			ModelAndView modelAndView = new ModelAndView("registration");
			modelAndView.addObject("validationError", "Email Format is wrong..");
			return modelAndView;
		}
		
		if(!mobileValidate(mobile)) {
			ModelAndView modelAndView = new ModelAndView("registration");
			modelAndView.addObject("validationError", "Mobile number format is wrong..");
			return modelAndView;
		}
		
		if(!passwordValidate(password)) {
			ModelAndView modelAndView = new ModelAndView("registration");
			modelAndView.addObject("validationError", "Password does not match with policy..Minimum 8 Chars,1 uppercase,1 digit,1 special character");
			return modelAndView;
		}
		
		UserAccount exist = service.findPasswordByEmail(email);
		
		if(exist != null) {
			ModelAndView modelAndView = new ModelAndView("registration");
			modelAndView.addObject("validationError", "Email address already registered with our system, please provide a new one");
			return modelAndView;
		}

		UserAccount user = new UserAccount();
		user.setEmail(email);
		user.setUserName(name);
		user.setPassword(password);
		user.setMobile(mobile);

		UserAccount createdUser = service.registerUser(user);

		if (createdUser != null) {
			ModelAndView modelAndView = new ModelAndView("welcome");
			modelAndView.addObject("name", createdUser.getUserName());
			return modelAndView;
		} else {
			ModelAndView modelAndView = new ModelAndView("registration");
			return modelAndView;
		}
	}

	@RequestMapping(value = "/forgetPassword", method = RequestMethod.GET)
	public String showForgetPasswordPage(ModelMap model) {
		return "forgetPassword";
	}

	@RequestMapping(value = "/forgetPassword", method = RequestMethod.POST)
	public String fetchCredential(ModelMap model, @RequestParam String email, @RequestParam String password) {
		//System.out.println("Email got is::"+email);
		
		UserAccount createdUser = service.findPasswordByEmail(email);
		
		//System.out.println("user got is::"+createdUser);
		if (createdUser != null) {
			
			if(!passwordValidate(password)) {
				model.addAttribute("failure", "New password does not match the policy..Minimum 8 Chars,1 uppercase,1 digit,1 special character");
				return "forgetPassword";
			}
			
			
			createdUser.setPassword(password);
			service.updatePassword(createdUser);
			model.addAttribute("sucess", "Your password has been reset");
			return "forgetPassword";
		} else {
			model.addAttribute("failure", "Password could not be reset as the email entered was wrong!!");
			return "forgetPassword";
		}
	}

	private boolean emailValidate(String weight) {
	Pattern pattern = Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
		return pattern.matcher(weight).matches();
    }

	private boolean mobileValidate(String weight) {
		 Pattern pattern = Pattern.compile("^([+]\\d{1,2}[- ]?)?\\d{8,11}$");
         return pattern.matcher(weight).matches();
    }
	
	private boolean passwordValidate(String password) {
		Pattern pattern = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$");
		return pattern.matcher(password).matches();
    }

}
