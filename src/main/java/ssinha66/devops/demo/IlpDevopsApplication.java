package ssinha66.devops.demo;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IlpDevopsApplication {

	public static void main(String[] args) {
		SpringApplication.run(IlpDevopsApplication.class, args);		
	}

}
