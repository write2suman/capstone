package ssinha66.devops.demo.service;

import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ssinha66.devops.demo.model.UserAccount;
import ssinha66.devops.demo.repository.UserRepository;

@Service
public class LoginService {
	
	@Autowired
	private UserRepository repository;

	public boolean validateUser(String name, String password) {
		// TODO Auto-generated method stub
		UserAccount user = repository.findByCredentials(name, password);
		return user != null ? true:false;
	}
	
	public UserAccount registerUser(UserAccount user) {
		
		return repository.save(user);
	}
	
	public UserAccount updatePassword(UserAccount user) {
		return repository.save(user);
	}
	
	public UserAccount findPasswordByEmail(String email) {
		return repository.findByEmail(email);
	}
	
	
}
