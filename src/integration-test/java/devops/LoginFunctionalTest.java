package devops;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxBinary;

import static org.junit.Assert.assertTrue;

import org.junit.*;
import org.openqa.selenium.By;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.AfterAll;
import org.openqa.selenium.WebElement;


@Category(IntegrationTest.class)
public class LoginFunctionalTest {
	static WebDriver driver;

	@BeforeClass
	public static void setUp() {
		FirefoxBinary firefoxBinary = new FirefoxBinary();
		firefoxBinary.addCommandLineOptions("--headless");
		System.setProperty("webdriver.gecko.driver", "/opt/data/geekodriver/geckodriver");
		FirefoxOptions firefoxOptions = new FirefoxOptions();
		firefoxOptions.setBinary(firefoxBinary);
		driver = new FirefoxDriver(firefoxOptions);
	}

	@AfterClass
	public static void cleanUp() {
		driver.quit();
	}

	@Test
	public void loginFailure() {
		driver.get("http://localhost:8080/bookstore/login");
		WebElement username = driver.findElement(By.name("name"));
		WebElement password = driver.findElement(By.name("password"));
		WebElement button = driver.findElement(By.xpath("/html/body/form/div/button"));
		username.sendKeys("suman");
		password.sendKeys("Suman@123");
		button.click();
		assertTrue(driver.getPageSource().contains("Invalid Credentials, please enter correct one.."));
		

	}
	@Test
	public void registrationSuccess() {
		driver.get("http://localhost:8080/bookstore/register");
		WebElement username = driver.findElement(By.name("name"));
		WebElement password = driver.findElement(By.name("password"));
		WebElement email = driver.findElement(By.name("email"));
		WebElement mobile = driver.findElement(By.name("mobile"));
		WebElement button = driver.findElement(By.xpath("/html/body/form/div/button"));
		username.sendKeys("suman");
		password.sendKeys("Suman@123");
		email.sendKeys("suman@yahoo.com");
		mobile.sendKeys("9830404512");
		button.click();
		//System.out.println(driver.getPageSource());
		assertTrue(driver.getPageSource().contains("Welcome to online bookstore"));		

	}
	@Test
	public void forgetPasswordSuccess() {
		driver.get("http://localhost:8080/bookstore/forgetPassword");
		WebElement password = driver.findElement(By.name("password"));
		WebElement email = driver.findElement(By.name("email"));
		WebElement button = driver.findElement(By.xpath("/html/body/form/div/button"));
		email.sendKeys("suman@yahoo.com");
		password.sendKeys("Wipro@123");		
		button.click();
		//System.out.println(driver.getPageSource());
		assertTrue(driver.getPageSource().contains("reset"));		

	}
}
